import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormGroup , FormControlName , Validators } from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
loginForm:FormGroup

  constructor(private FormBuilder:FormBuilder) { }

  ngOnInit() {
    this.createLoginForm()
  }

  createLoginForm(){
    
    this.loginForm = this.FormBuilder.group({
      username:'',
      password:''
    })

  }


  submit(){
    console.log(this.loginForm.value)
  }

}
