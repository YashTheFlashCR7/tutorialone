import { Component } from '@angular/core';
import { addAllToArray } from '@angular/core/src/render3/util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'yashlin';
  heading:string = 'This is our first tutorial'
  contentArray:any = [] // declared an array 

  addAllToArray(content:string){
    this.contentArray.push(content)
    console.log(this.contentArray)
  }

  


}
